#!/usr/bin/python3

import RPi.GPIO as gpio
import time

pinToRelay=3
gpio.setmode(gpio.BOARD)
gpio.setup(pinToRelay.gpio.OUT)
gpio.output(pinToRelay,True)
print("Rebooting...")
time.sleep(5)
gpio.output(pinToRelay,False)
print("Done!")
