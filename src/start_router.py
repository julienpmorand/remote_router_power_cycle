#!/usr/bin/python3
import RPi.GPIO as gpio

pinToRelay=3
gpio.setmode(gpio.BOARD)
gpio.setup(pinToRelay,gpio.OUT)
gpio.output(pinToRelay,False)
