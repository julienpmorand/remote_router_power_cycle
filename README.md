This little setup allows you to reboot your router remotely if it ever crashes. This may be useful for you if you need to connect to a home VPN or connect to a local device on your network whilst you are away while your router decided to crash on you.

# Usage

Whenever you desire to reboot your router remotely, simply `ssh` to your Raspberry Pi using your external IP address (see installation step 10).

```bash
ssh pi@<YOUR_EXTERNAL_IP_ADDRESS>
```

And then execute the following command

```bash
./home/pi/reboot_router.py
```

# Bill of Materials

| Part | Quantity |
| ---- | -------- |
| [Raspberry Pi Zero](https://www.raspberrypi.org/products/raspberry-pi-zero/) | 1 |
| [16 GB Micro SD card](https://www.amazon.com/16GB-Sandisk-microSD-Memory-Adapter/dp/B00FZVQPBC/ref=sr_1_8?dchild=1&keywords=micro+sd&qid=1626632764&sr=8-8) | 1 |
| [5-12V Relay Module](https://www.banggood.com/3Pcs-5V-Relay-5-12V-TTL-Signal-1-Channel-Module-High-Level-Expansion-Board-p-1178211.html?cur_warehouse=CN&rmmds=search) | 1 |
| [Micro USB AC/DC adapter](https://www.amazon.com/Travel-Charger-Adapter-Samsung-Galaxy/dp/B0117O020U/ref=sr_1_4?dchild=1&keywords=micro+usb+ac&qid=1626632835&sr=8-4) | 1 |
| [Male-Female Dupont Wire](https://www.banggood.com/40pcs-20cm-Male-To-Female-Jumper-Cable-Dupont-Wire-p-973822.html?cur_warehouse=CN&rmmds=search) | 3 Wires |
| [Female 5mm Barrel Connector](https://www.banggood.com/12V-DC-Power-Male-Female-Plug-Jack-Adapter-Connector-Socket-for-CCTV-5_52_5mm-p-1118094.html?cur_warehouse=CN&ID=516399&rmmds=search) | 1 |
| [Male 5mm Barrel Connector](https://www.banggood.com/12V-DC-Power-Male-Female-Plug-Jack-Adapter-Connector-Socket-for-CCTV-5_52_5mm-p-1118094.html?cur_warehouse=CN&ID=516399&rmmds=search) | 1 |
| [16 AWG or more wires](https://www.banggood.com/1M-8-or-10-or-12-or-14-or-16-or-18-or-20-or-22-or-24-or-26-AWG-Silicone-Wire-SR-Wire-p-921159.html?cur_warehouse=USA&ID=512908&rmmds=search) | 2 Wires |


# Installation

## Circuit

Connect he circuit as follows

![](figures/diag.png)

![](figures/schematic.jpg)

## Raspberry Pi Configuration

1. Download and install [Raspberry Pi OS](https://www.raspberrypi.org/downloads/raspberry-pi-os/) and flash it on an empty micro SD card using a program like [Balena Etcher](https://www.balena.io/etcher/).

2. Boot the Raspberry Pi and connect it to your network on the same level as the modem like shown in the following network figure

  ![](figures/network.png)

3. [Enable `ssh` on the Raspberry Pi](https://www.raspberrypi.org/documentation/remote-access/ssh/), ideally by setting up [certificate authentication rather than password authentication](https://www.raspberrypi.org/documentation/remote-access/ssh/passwordless.md).

4. Follow the instructions of your Internet Service Provider's (ISP) modem/router combo to port forward port `22` to your Raspberry Pi. In this example, port `22` would be forwarded to `192.168.0.3`.

5. (optional) Follow the instructions of your Internet Service Provider's (ISP) modem/router combo to reserve the IPv4 address of the Raspberry Pi to its MAC address to make sure its IP remains constant (see DHCP reservations).

6. Connect to the Raspberry Pi and copy the `src/reboot_router.py` and `src/start_router.py` files in the home (`~/` or `/home/pi/`) directory.

7. Make sure the `reboot_router.py` and `start_router.py` files are executable by performing

  ```bash
chmod +x /home/pi/reboot_router.py
chmod +x /home/pi/start_router.py
```

8. Run the `start_router.py` file on startup. To do that, type

  ```bashrc
sudo crontab -e
```
You may use nano as the text editor if prompted and add the following line at the end of the document:
```
@reboot /home/pi/reboot_router/start_router.py
```
CTRL+X will exit and say `y` to save when prompted.

9. Reboot the Raspberry Pi.

10. Make sure you know your external IP address by visiting [whatsmyip.org](https://whatsmyip.org)
